package tarjetapresentacion;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Reyes Arellano Angélica 
 * 
 */
public class Persona {
    
    private String nombre;
    private short edad;
    private char sexo;
    private String nacionalidad;
    private String ocupacion;
    private String celular;
    private String mail;
    private String estadocivil;
    
    
    public String getEstadocivil(){
        return estadocivil;
    }
    
    public String getCelular() {
        return celular;
    }

    public String getMail() {
        return mail;
    }
    public void setEstadocivil(String estadocivil){
        this.estadocivil=estadocivil;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Persona(String nomArch){
        this.Leer(nomArch);
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }
    
    private void Leer(String archivo){
        try{//lo que vamos intentar hacer
            FileReader fr= new FileReader(archivo);//lee el archivo
            BufferedReader br= new BufferedReader(fr);//recibe el filereader
            
            String nombre = br.readLine();//nos regresa un string 
            this.nombre = nombre;
            String edad = br.readLine();
            this.edad = Short.parseShort(edad);
            
            String temp=  br.readLine().substring(0,1).toLowerCase();
            if( temp.equals("h") || temp.equals("m")){//toLowercase cambia las mayusculas en minusculas
                this.sexo=temp.charAt(0);
            }
            else{
                System.out.println("Error con el campo sexo.\nValores aceptados:"
                        + " m, h, hombre o mujer");
                System.exit(0);
            }
            this.nacionalidad=br.readLine();
            this.ocupacion=br.readLine();
            this.celular=br.readLine();
            this.mail=br.readLine();
            this.estadocivil=br.readLine();
            br.close();
        }
    
        catch(Exception e){//lo que pasa si no se puede hacer todo lo de try
            System.err.println("Error en el archivo");
            System.exit(0);//va a terminar el programa 
        }
    }
    public void GeneraTarjeta(){
        String cadena=generaCadena();
        System.out.println(cadena);
        try{
            FileWriter fw=new  FileWriter("TarjetaDe"+this.nombre);
            BufferedWriter bw=new BufferedWriter(fw);
            bw.write(cadena);
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.err.println("Error con la escritura");
        }
    }
    
    private String generaCadena() {
        //Va a guardar cada líbea del archivo en el arreglo genera
        String genera[]=new String[8];
        String gen="";
        
        genera[0]="Mi nombre es "+this.nombre;
        genera[1]="Mi edad es "+this.edad;
        
        String Sexo="";
        if(this.sexo == 'h'){
            Sexo="Hombre";
        }
        else{
            Sexo="Mujer";
        }
       
        genera[2]="Soy "+Sexo;
        genera[3]="Mi nacionalidad es "+this.nacionalidad;
        genera[4]="Soy "+this.ocupacion;
        genera[5]="Mi número de celular para contactarme es "+this.celular;
        genera[6]="Mi mail para contactarme es "+this.mail;
        genera[7]="Mi estado civil es "+this.estadocivil;
        
        
        //Busca el numero más largo de linea de cada arreglo para tomarlo como base
        int largo=0;
        for(String arreglos : genera){
            largo=Math.max(largo, arreglos.length());
            //Se le suma uno más para que haya espacios de sobra
            largo= largo+1;
        }
        
        //Va a imprimir como un "titulo": Tarjeta de concatenando el nombre
        System.out.printf("Tarjeta de presentación de %s%n%n", this.nombre);
        //Va concatenar las diferentes cadenas
        for(String parrafo : genera){
            
            //Va a concatenar el primer | antes de los guiones
            gen += "|";
            //Va a concatenar guiones del mismo tamaño que la cadena más larga
            for(int k = 0; k < largo; k++){
                gen += "-";
            }
            //Va a concatenar el ultimo | después de los guiones
            gen+="|";
            //Concatena un salto de linea y cada cadena que corresponda al arreglo
            gen += "\n|" + parrafo;
                
            //Va a concatenar los espacios antes del | y al acabar cada parrafo
            for(int i = 0; i < largo-(parrafo.length()); i++)
            gen+=" ";
            //Va a concatenar el ultimo | y dar un salto de línea
            gen+="|\n";
        
        }
            //Va a concatenar el primer | de los últimos guiones
            gen+="|";
            //Va a concatenar los ultimos - hasta abajo del String
            for(int j = 0; j < largo; j++){
            gen+="-";
            }
            //Va a concatenar el ultimo | de los ultimos guiones
            gen+="|";
        
        //Regresa la cadena
        return(gen);
        
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }


    
}
